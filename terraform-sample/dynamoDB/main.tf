resource "aws_dynamodb_table" "dynamodb" {

    point_in_time_recovery {
      enabled = true
    }
    
    server_side_encryption {
      kms_key_arn= "arn:{redacted}"
    }
}