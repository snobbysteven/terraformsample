resource "aws_s3_bucket" "s3" {
  bucket = "my-tf-remediation-bucket"
  acl    = "private"

  tags = {
    Name        = "My bucket1"
    Environment = "Dev"
  }
  
  resource "aws_s3_bucket_cors_configuration" "example" {
    bucket = aws_s3_bucket.example.bucket
  
    cors_rule {
      allowed_methods = ["POST]
    }
    
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:{redacted}"
      }
    }
  }
}
